const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

const { asyncWrapper } = require('./middlewares/helpers');
const { authMiddleware } = require('./middlewares/authMiddleware');
const { postNote, getAllNotes, getNote, updateNote, checkNote, deleteNote } = require('../controllers/noteController');


router.get('/', authMiddleware, asyncWrapper(getAllNotes));
router.post('/', authMiddleware, asyncWrapper(postNote));
router.get('/:id', authMiddleware, asyncWrapper(getNote));
router.put('/:id', authMiddleware, asyncWrapper(updateNote));
router.patch('/:id', authMiddleware, asyncWrapper(checkNote));
router.delete('/:id', authMiddleware, asyncWrapper(deleteNote));

module.exports = router;