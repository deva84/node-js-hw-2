const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

const { asyncWrapper } = require('./middlewares/helpers');
const { validateRegistration } = require('./middlewares/validation');
const { login, registration } = require('./../controllers/authController');

router.post('/register', asyncWrapper(validateRegistration), asyncWrapper(registration));
router.post('/login', asyncWrapper(login));

module.exports = router;
