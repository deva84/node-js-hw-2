const Joi = require('joi');

async function validateRegistration(req, res, next) {
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .required(),

        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    });

    try {
        await schema.validateAsync(req.body);
        next();
        
    } catch(err) {

        res.status(400).json({message: err.message});
    }    
}

module.exports = { validateRegistration };
