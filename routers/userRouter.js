const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./middlewares/helpers');
const { authMiddleware } = require('./middlewares/authMiddleware');
const { getUserData, changePassword, deleteUserData } = require('../controllers/userController');

router.get('/me', authMiddleware, asyncWrapper(getUserData));
router.patch('/me', authMiddleware, asyncWrapper(changePassword));
router.delete('/me', authMiddleware, asyncWrapper(deleteUserData));

module.exports = router;