const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        default: false
    },
    text: {
        type: String
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports.noteModel = mongoose.model('noteModel', noteSchema);