require('dotenv').config();
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET || 'yahoo';
const { noteModel } = require('../models/noteModel');

async function postNote(req, res) {
    const text = req.body.text;

    if (!text) {
        return res.status(400).json({message: 'You can\'t post an empty note'});
    }

    if (req.user) {
        const note = new noteModel({
            userId: req.user._id,
            text
        });
        await note.save();
        res.status(200).json({ message: 'Success' });
    
    } else {
        res.status(400).json('You should pass authorization first');
    }
    
}

async function getAllNotes(req, res) {
    const { skip, limit } = req.query;

    if (req.user) {

        try {
            const requestedOptions = {
              skip: parseInt(skip),
              limit: parseInt(limit)
            };
        
            const notes = await noteModel.find(
              {},
              ['userId', 'text', 'completed', 'createdDate'],
              requestedOptions
            );

            res.status(200).json({notes: notes, skip, limit});
        
          } catch(err) {
            res.status(400).json({message: err.message});
          }
    
    } else {
        res.status(400).json('You should pass authorization first');
    }  
}

async function getNote(req, res) {
    const noteId = req.params.id;

    if (req.user) {

        try {    
            const note = await noteModel.findById(noteId, ['userId', 'text', 'completed', 'createdDate']);
            res.status(200).json({note: note});
        
          } catch(err) {
            res.status(400).json({message: err.message});
          }
    
    } else {
        res.status(400).json('You should pass authorization first');
    }  
}

async function updateNote(req, res) {
    const noteId = req.params.id;
    const newText = req.body.text;

    if (!newText) {
        return res.status(400).json({message: 'You can\'t post an empty note'});
    }

    if (req.user) {

        try {
            const note = await noteModel.findByIdAndUpdate(noteId, {$set: {text: newText}});
            res.status(200).json({message: 'Success'});
        
          } catch(err) {
            res.status(400).json({message: err.message});
          }
    
    } else {
        res.status(400).json('You should pass authorization first');
    }  
}

async function checkNote(req, res) {
    const noteId = req.params.id;

    if (req.user) {

        try {
            const note = await noteModel.findById(noteId);
            await noteModel.findByIdAndUpdate(noteId, {$set: {completed: !note.completed}});
            res.status(200).json({message: 'Success'});
        
          } catch(err) {
            res.status(400).json({message: err.message});
          }
    
    } else {
        res.status(400).json('You should pass authorization first');
    }  
}

async function deleteNote(req, res) {
    const noteId = req.params.id;

    if (req.user) {

        try {
            await noteModel.findByIdAndDelete(noteId);
            res.status(200).json({message: 'Success'});
            
          } catch(err) {
            res.status(500).json({message: err.message});
          }
    
    } else {
        res.status(400).json('You should pass authorization first');
    }  
}

module.exports = { postNote, getAllNotes, getNote, updateNote, checkNote, deleteNote };