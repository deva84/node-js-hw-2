require('dotenv').config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET || 'yahoo';
const { userModel } = require('../models/userModel');

async function registration(req, res) {
    const { username, password } = req.body;

    if (!password) {
        return res.status(400).json({message: `Password are mandatory`})
    }

    if (!username) {
        return res.status(400).json({message: `Username is mandatory`})
    }

    const duplicate = await userModel.findOne({username});

    if (duplicate) {
        return res.status(400).json({message: `User ${username} already exists`});
    }

    const user = new userModel({
        username,
        password: await bcrypt.hash(password, 10)
    });

    try {
        await user.save();
        res.status(200).json({message: `User ${username} has been successfully created`});
    
    } catch (err) {
        res.status(500).json({message: `Internal server error`});
    }   
}

async function login (req, res) {
    const { username, password } = req.body;

    try {
        const user = await userModel.findOne({username});

        if (!user) {

            return res.status(400).json({message: `User ${username} was not found`});
        }

        if (!password) {

            return res.status(400).json({message: `Password is mandatory`});
        }

        if ( !(await bcrypt.compare(password, user.password)) ) {
            return res.status(400).json({message: 'Wrong password'});
        }
        
        const token = jwt.sign({ username: user.username, _id: user._id}, jwtSecret);
        
        res.status(200).json( {message: 'Success', jwt_token: token} );
    
    } catch(err) {
        res.status(500).json('Internal server error')
    }

}

module.exports = { registration, login };