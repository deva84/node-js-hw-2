require('dotenv').config();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET || 'yahoo';
const { userModel } = require('../models/userModel');

async function getUserData(req, res) {
    
    if (req.user) {
        const _id = req.user._id;
        const data = await userModel.findOne({_id});

        if (data) {
            res.status(200).json({user: {_id: data._id, username: data.username, createdDate: data.createdDate}});
        }  else {
            res.status(400).json('User was not found');
        }

    } else {
        res.status(400).json('You should pass authorization first');
    }
}

async function changePassword(req, res) {
    const { oldPassword, newPassword } = req.body;

    if ((!oldPassword) || (!newPassword)) {
        return res.status(400).json({message: 'Provide necessary data: old and new passwords'})
    }
    
    if (req.user) {
        const _id = req.user._id;
        const data = await userModel.findOne({_id});

        if (data) {
            const oldPasswordDB = data.password;

            if ((await bcrypt.compare(oldPassword, oldPasswordDB))) {
                data.password = await bcrypt.hash(newPassword, 10);
                await data.save();
                res.status(200).json({message: 'Your password has been updated'});

            } else {
                res.status(400).json({message: 'Old password doesn\'t match the saved one'});
            }

        }  else {
            res.status(400).json('User was not found');
        }

    } else {
        res.status(400).json('You should pass authorization first');
    }
    
}

async function deleteUserData(req, res) {
    
    if (req.user) {
        const _id = req.user._id;
        const data = await userModel.findOne({_id});

        if (data) {
            try {
               data.remove();
               res.status(200).json({message: 'Success'});

            } catch(err) {
                res.status(400).json({message: 'We\'ve got an error while deleting data: ', err})
            }

        }  else {
            res.status(400).json('User was not found');
        }

    } else {
        res.status(400).json('You should pass authorization first');
    }
}

module.exports = { getUserData, changePassword, deleteUserData };