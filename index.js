require('dotenv').config();
const port = process.env.PORT || 8080;
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');
const userRouter = require('./routers/userRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/users', userRouter);
app.use('/api/notes', noteRouter);
app.use('/api/auth', authRouter);

class CustomError extends Error {
  constructor(message = 'Custom error') {
    super(message);
    errorCode = 400;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof CustomError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://test:hjlbyf1@cluster0.kfbem.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  });

  app.listen(port, () => {
    console.log(`Server is working on port ${port}...`);
  });
}

start();